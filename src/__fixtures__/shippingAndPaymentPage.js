import {
    Box,
    Text,
    Flex,
    Heading,
    Link,
    FormLabel,
    Input,
    FormControl,
    Stack,
    Icon,
    Tooltip,
    Button,
    Select,
    Radio,
    InputGroup,
    InputRightElement,
    FormErrorMessage,
} from "@chakra-ui/core"
import React, { useState } from "react"
import { Formik, Form, Field } from "formik"
import cardValidator from "card-validator"
import * as Yup from "yup"
import { Header, SiteWrapper, Steps, Step, Footer, CartSummaryBox } from "../"
import { tax, discount, shippingInfo, mockProducts } from "../utils/mockData"

const UserInfoFormValidatorSchema = Yup.object().shape({
    firstName: Yup.string()
        .min(2, "Too Short!")
        .max(50, "Too Long!")
        .required("Required"),
    lastName: Yup.string()
        .min(2, "Too Short!")
        .max(50, "Too Long!")
        .required("Required"),
    email: Yup.string()
        .email("Invalid email")
        .required("Required"),
})

const CreditCardValidatorSchema = Yup.object().shape({
    cardNumber: Yup.string()
        .test(
            "test-number",
            "card number is invalid",
            value => cardValidator.number(value).isValid
        )
        .required("Required"),
    cardExpDate: Yup.string().test(
        "test-number",
        "exp date is invalid",
        value => cardValidator.expirationDate(value).isValid
    ),
    cardCVV: Yup.string().test(
        "test-number",
        "cvv is invalid",
        value => cardValidator.cvv(value).isValid
    ),
})

const PaymentMethods = () => {
    const [currentOption, setCurrentOption] = useState("card")
    return (
        <Box mb={10}>
            <Heading as="h2" fontSize="lg">
                Payment
            </Heading>
            <Text>All transactions are secure and encrypted.</Text>
            <Box my={4}>
                <Box
                    border="solid 1px"
                    borderColor="gray.300"
                    paddingY={4}
                    borderRadius="md"
                >
                    <Box borderBottom="solid 1px" borderColor="gray.300">
                        <Radio
                            value="card"
                            isChecked={currentOption === "card" ? true : false}
                            onChange={e => setCurrentOption(e.target.value)}
                            paddingLeft={4}
                            paddingBottom={4}
                        >
                            Credit Card
                        </Radio>
                        {currentOption === "card" ? (
                            <Box padding={4} bg="gray.100">
                                <CreditCardForm />
                            </Box>
                        ) : null}
                    </Box>
                    <Box paddingX={4} paddingTop={4}>
                        <Radio
                            value="checkmo"
                            isChecked={
                                currentOption === "checkmo" ? true : false
                            }
                            onChange={e => setCurrentOption(e.target.value)}
                        >
                            Check / Money order
                        </Radio>
                        {currentOption === "checkmo" ? (
                            <Box> Money Order </Box>
                        ) : null}
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}

const CreditCardForm = () => {
    const prettyCardNumber = (cardNumber, card) => {
        if (card) {
            var offsets = [].concat(0, card.gaps, cardNumber.length)
            var components = []

            for (var i = 0; offsets[i] < cardNumber.length; i++) {
                var start = offsets[i]
                var end = Math.min(offsets[i + 1], cardNumber.length)
                components.push(cardNumber.substring(start, end))
            }

            return components.join(" ")
        }

        return cardNumber
    }

    const handleCardNumber = value => {
        let cardNumber = value
            .split(" ")
            .filter(item => typeof parseInt(item) === "number")
            .join("")
        return prettyCardNumber(
            cardNumber,
            cardValidator.number(cardNumber).card
        )
    }
    return (
        <Formik
            initialValues={{
                cardNumber: "",
                cardExpDate: "",
                cardCVV: "",
            }}
            validationSchema={CreditCardValidatorSchema}
            onSubmit={values => {
                console.log(values)
            }}
        >
            {({ handleChange, setFieldValue }) => {
                return (
                    <Form>
                        <Box width="100%" paddingRight={6}>
                            <Field name="cardNumber">
                                {({ field, form }) => {
                                    return (
                                        <FormControl
                                            isInvalid={
                                                form.errors.cardNumber &&
                                                form.touched.cardNumber
                                            }
                                        >
                                            <Stack
                                                isInline
                                                alignItems="center"
                                                justifyContent="space-between"
                                            >
                                                <FormLabel htmlFor="cardNumber">
                                                    Card number
                                                </FormLabel>
                                                <Text>
                                                    {cardValidator.number(
                                                        form.values.cardNumber
                                                    ).card
                                                        ? cardValidator.number(
                                                              form.values
                                                                  .cardNumber
                                                          ).card.niceType
                                                        : null}
                                                </Text>
                                            </Stack>
                                            <InputGroup>
                                                <Input
                                                    {...field}
                                                    onChange={e => {
                                                        handleChange(e)
                                                        setFieldValue(
                                                            "cardNumber",
                                                            handleCardNumber(
                                                                e.target.value
                                                            )
                                                        )
                                                    }}
                                                    id="cardNumber"
                                                />
                                                <InputRightElement
                                                    children={
                                                        <Tooltip
                                                            hasArrow
                                                            label="All transactions are secure and encrypted."
                                                            placement="top"
                                                        >
                                                            <Icon
                                                                name="lock"
                                                                color="gray.500"
                                                            />
                                                        </Tooltip>
                                                    }
                                                />
                                            </InputGroup>
                                            <FormErrorMessage>
                                                {form.errors.cardNumber}
                                            </FormErrorMessage>
                                        </FormControl>
                                    )
                                }}
                            </Field>
                        </Box>
                        <Box width="100%" paddingRight={6} my={4}>
                            <FormControl>
                                <FormLabel
                                    htmlFor="name-on-card"
                                    fontFamily="body"
                                >
                                    Name on card
                                </FormLabel>
                                <Input />
                            </FormControl>
                        </Box>
                        <Stack isInline my={4}>
                            <Box width={1 / 2} paddingRight={6}>
                                <Field name="cardExpDate">
                                    {({ field, form }) => {
                                        return (
                                            <FormControl
                                                isInvalid={
                                                    form.errors.cardExpDate &&
                                                    form.touched.cardExpDate
                                                }
                                            >
                                                <FormLabel
                                                    htmlFor="cardExpDate"
                                                    fontFamily="body"
                                                >
                                                    Expiration date (MM/YY)
                                                </FormLabel>
                                                <Input
                                                    {...field}
                                                    id="cardExpDate"
                                                />
                                                <FormErrorMessage>
                                                    {form.errors.cardExpDate}
                                                </FormErrorMessage>
                                            </FormControl>
                                        )
                                    }}
                                </Field>
                            </Box>
                            <Box width={1 / 2} paddingRight={6}>
                                <Field name="cardCVV">
                                    {({ field, form }) => {
                                        return (
                                            <FormControl
                                                isInvalid={
                                                    form.errors.cardCVV &&
                                                    form.touched.cardCVV
                                                }
                                            >
                                                <FormLabel
                                                    htmlFor="cardCVV"
                                                    fontFamily="body"
                                                >
                                                    Security Code
                                                </FormLabel>
                                                <InputGroup>
                                                    <Input
                                                        {...field}
                                                        id="cardCVV"
                                                    />
                                                    <InputRightElement
                                                        children={
                                                            <Tooltip
                                                                hasArrow
                                                                label="3-digit security code usually found on the back of your card. American Express cards have a 4-digit code located on the front. "
                                                                placement="top"
                                                            >
                                                                <Icon
                                                                    name="question"
                                                                    color="gray.500"
                                                                />
                                                            </Tooltip>
                                                        }
                                                    />
                                                </InputGroup>
                                                <FormErrorMessage>
                                                    {form.errors.cardCVV}
                                                </FormErrorMessage>
                                            </FormControl>
                                        )
                                    }}
                                </Field>
                            </Box>
                        </Stack>
                    </Form>
                )
            }}
        </Formik>
    )
}
const AddressFormGroup = () => {
    return (
        <Formik
            initialValues={{
                firstName: "",
                lastName: "",
                email: "",
            }}
            validationSchema={UserInfoFormValidatorSchema}
            onSubmit={values => {
                // same shape as initial values
                console.log(values)
            }}
        >
            {({ errors, touched }) => (
                <Form>
                    <Box>
                        <Stack isInline my={4}>
                            <Box width={1 / 2} paddingRight={6}>
                                <Field name="firstName">
                                    {({ field, form }) => (
                                        <FormControl
                                            isInvalid={
                                                form.errors.firstName &&
                                                form.touched.firstName
                                            }
                                        >
                                            <FormLabel htmlFor="firstName">
                                                First name
                                            </FormLabel>
                                            <Input
                                                {...field}
                                                id="firstName"
                                                placeholder="First name"
                                            />
                                            <FormErrorMessage>
                                                {form.errors.firstName}
                                            </FormErrorMessage>
                                        </FormControl>
                                    )}
                                </Field>
                            </Box>
                            <Box width={1 / 2} paddingRight={6}>
                                <Field name="lastName">
                                    {({ field, form }) => (
                                        <FormControl
                                            isInvalid={
                                                form.errors.lastName &&
                                                form.touched.lastName
                                            }
                                        >
                                            <FormLabel htmlFor="lastName">
                                                First name
                                            </FormLabel>
                                            <Input
                                                {...field}
                                                id="lastName"
                                                placeholder="Last name"
                                            />
                                            <FormErrorMessage>
                                                {form.errors.lastName}
                                            </FormErrorMessage>
                                        </FormControl>
                                    )}
                                </Field>
                            </Box>
                        </Stack>
                    </Box>
                    <Box my={4} width="100%" paddingRight={6}>
                        <FormControl>
                            <FormLabel
                                htmlFor="company"
                                fontFamily="body"
                                display="flex"
                            >
                                Company
                                <Text color="gray.300" ml={2}>
                                    (optional)
                                </Text>
                            </FormLabel>
                            <Input type="text" id="company" />
                        </FormControl>
                    </Box>
                    <Box my={4} width="100%" paddingRight={6}>
                        <FormControl>
                            <FormLabel htmlFor="address" fontFamily="body">
                                Address
                            </FormLabel>
                            <Input type="text" id="address" />
                        </FormControl>
                    </Box>
                    <Box my={4} width="100%" paddingRight={6}>
                        <FormControl>
                            <FormLabel
                                htmlFor="address-line-2"
                                fontFamily="body"
                                display="flex"
                            >
                                Address line 2
                                <Text color="gray.300" ml={2}>
                                    (optional)
                                </Text>
                            </FormLabel>
                            <Input type="text" id="address-line-2" />
                        </FormControl>
                    </Box>
                    <Box my={4} width="100%" paddingRight={6}>
                        <FormControl>
                            <FormLabel htmlFor="city" fontFamily="body">
                                City
                            </FormLabel>
                            <Input type="text" id="city" />
                        </FormControl>
                    </Box>
                    <Stack isInline mt={4}>
                        <Box width={1 / 3} paddingRight={6}>
                            <FormControl>
                                <FormLabel
                                    htmlFor="country-region"
                                    fontFamily="body"
                                >
                                    Country/Region
                                </FormLabel>
                                <Select
                                    placeholder="United States"
                                    id="country-region"
                                >
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </Select>
                            </FormControl>
                        </Box>
                        <Box width={1 / 3} paddingRight={6}>
                            <FormControl>
                                <FormLabel htmlFor="state" fontFamily="body">
                                    State
                                </FormLabel>
                                <Select placeholder="California" id="state">
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </Select>
                            </FormControl>
                        </Box>
                        <Box width={1 / 3} paddingRight={6}>
                            <FormControl>
                                <FormLabel htmlFor="zip-code" fontFamily="body">
                                    Zip code
                                </FormLabel>
                                <Input type="number" id="zip-code" />
                            </FormControl>
                        </Box>
                    </Stack>
                </Form>
            )}
        </Formik>
    )
}

const BillingAdress = () => {
    const [currentOption, setCurrentOption] = useState(
        "same_as_shipping_address"
    )
    return (
        <Box mb={10}>
            <Heading as="h2" fontSize="lg">
                Billing address
            </Heading>
            <Text>
                Select the address that matches your card or payment method.
            </Text>
            <Box my={4}>
                <Box
                    border="solid 1px"
                    borderColor="gray.300"
                    borderRadius="md"
                    overflow="hidden"
                >
                    <Box borderBottom="solid 1px" borderColor="gray.300">
                        <Radio
                            value="same_as_shipping_address"
                            isChecked={
                                currentOption === "same_as_shipping_address"
                                    ? true
                                    : false
                            }
                            onChange={e => setCurrentOption(e.target.value)}
                            padding={4}
                        >
                            Same as shipping address
                        </Radio>
                    </Box>
                    <Box>
                        <Radio
                            value="different_billing_address"
                            isChecked={
                                currentOption === "different_billing_address"
                                    ? true
                                    : false
                            }
                            onChange={e => setCurrentOption(e.target.value)}
                            padding={4}
                        >
                            Use a different billing address
                        </Radio>
                        {currentOption === "different_billing_address" ? (
                            <Box
                                bg="gray.100"
                                paddingX={6}
                                paddingBottom={6}
                                paddingTop={2}
                            >
                                <AddressFormGroup />
                            </Box>
                        ) : null}
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}

export default {
    "guest-shipping-step": (
        <>
            <Header />
            <Box bg="white">
                <SiteWrapper>
                    <Box my={4}>
                        <Steps current={1}>
                            <Step title="Shipping" />
                            <Step title="Payment" />
                            <Step title="Order Review" />
                        </Steps>
                    </Box>
                    <Flex>
                        <Box width={[3 / 4]}>
                            <Box mb={10}>
                                <Flex
                                    alignItems="center"
                                    justifyContent="space-between"
                                >
                                    <Heading as="h2" fontSize="lg">
                                        Account
                                    </Heading>
                                    <Text>
                                        Already have an account?{" "}
                                        <Link href="#" color="blue.700">
                                            Log in
                                        </Link>
                                    </Text>
                                </Flex>
                                <Stack isInline my={4}>
                                    <Box width={1 / 2} paddingRight={6}>
                                        <FormControl>
                                            <FormLabel
                                                htmlFor="first-name"
                                                fontFamily="body"
                                            >
                                                First Name
                                            </FormLabel>
                                            <Input
                                                type="text"
                                                id="first-name"
                                            />
                                        </FormControl>
                                    </Box>
                                    <Box width={1 / 2} paddingRight={6}>
                                        <FormControl>
                                            <FormLabel
                                                htmlFor="last-name"
                                                fontFamily="body"
                                            >
                                                Last Name
                                            </FormLabel>
                                            <Input type="text" id="last-name" />
                                        </FormControl>
                                    </Box>
                                </Stack>
                                <Stack isInline my={4}>
                                    <Box width={1 / 2} paddingRight={6}>
                                        <FormControl>
                                            <FormLabel
                                                htmlFor="email"
                                                fontFamily="body"
                                            >
                                                Email
                                            </FormLabel>
                                            <Input type="email" id="email" />
                                        </FormControl>
                                    </Box>
                                    <Box width={1 / 2} paddingRight={6}>
                                        <FormControl>
                                            <FormLabel
                                                htmlFor="phone"
                                                fontFamily="body"
                                                display="flex"
                                                alignItems="center"
                                            >
                                                Phone
                                                <Text ml={2} color="gray.300">
                                                    (optional)
                                                </Text>
                                                <Tooltip
                                                    hasArrow
                                                    label="In case we need to contact you about your order"
                                                    placement="top"
                                                >
                                                    <Button variant="link">
                                                        <Icon name="question" />
                                                    </Button>
                                                </Tooltip>
                                            </FormLabel>
                                            <Input type="number" id="phone" />
                                        </FormControl>
                                    </Box>
                                </Stack>
                            </Box>
                            <Box mb={10}>
                                <Heading as="h2" fontSize="lg">
                                    Shipping address
                                </Heading>

                                <Box my={4} width="100%" paddingRight={6}>
                                    <FormControl>
                                        <FormLabel
                                            htmlFor="company"
                                            fontFamily="body"
                                            display="flex"
                                        >
                                            Company
                                            <Text color="gray.300" ml={2}>
                                                (optional)
                                            </Text>
                                        </FormLabel>
                                        <Input type="text" id="company" />
                                    </FormControl>
                                </Box>
                                <Box my={4} width="100%" paddingRight={6}>
                                    <FormControl>
                                        <FormLabel
                                            htmlFor="address"
                                            fontFamily="body"
                                        >
                                            Address
                                        </FormLabel>
                                        <Input type="text" id="address" />
                                    </FormControl>
                                </Box>
                                <Box my={4} width="100%" paddingRight={6}>
                                    <FormControl>
                                        <FormLabel
                                            htmlFor="address-line-2"
                                            fontFamily="body"
                                            display="flex"
                                        >
                                            Address line 2
                                            <Text color="gray.300" ml={2}>
                                                (optional)
                                            </Text>
                                        </FormLabel>
                                        <Input
                                            type="text"
                                            id="address-line-2"
                                        />
                                    </FormControl>
                                </Box>
                                <Box my={4} width="100%" paddingRight={6}>
                                    <FormControl>
                                        <FormLabel
                                            htmlFor="city"
                                            fontFamily="body"
                                        >
                                            City
                                        </FormLabel>
                                        <Input type="text" id="city" />
                                    </FormControl>
                                </Box>
                                <Stack isInline my={4}>
                                    <Box width={1 / 3} paddingRight={6}>
                                        <FormControl>
                                            <FormLabel
                                                htmlFor="country-region"
                                                fontFamily="body"
                                            >
                                                Country/Region
                                            </FormLabel>
                                            <Select
                                                placeholder="United States"
                                                id="country-region"
                                            >
                                                <option value="option1">
                                                    Option 1
                                                </option>
                                                <option value="option2">
                                                    Option 2
                                                </option>
                                                <option value="option3">
                                                    Option 3
                                                </option>
                                            </Select>
                                        </FormControl>
                                    </Box>
                                    <Box width={1 / 3} paddingRight={6}>
                                        <FormControl>
                                            <FormLabel
                                                htmlFor="state"
                                                fontFamily="body"
                                            >
                                                State
                                            </FormLabel>
                                            <Select
                                                placeholder="California"
                                                id="state"
                                            >
                                                <option value="option1">
                                                    Option 1
                                                </option>
                                                <option value="option2">
                                                    Option 2
                                                </option>
                                                <option value="option3">
                                                    Option 3
                                                </option>
                                            </Select>
                                        </FormControl>
                                    </Box>
                                    <Box width={1 / 3} paddingRight={6}>
                                        <FormControl>
                                            <FormLabel
                                                htmlFor="zip-code"
                                                fontFamily="body"
                                            >
                                                Zip code
                                            </FormLabel>
                                            <Input
                                                type="number"
                                                id="zip-code"
                                            />
                                        </FormControl>
                                    </Box>
                                </Stack>
                            </Box>
                            <Box mb={10}>
                                <Stack
                                    isInline
                                    my={4}
                                    alignItems="center"
                                    justifyContent="space-between"
                                >
                                    <Button>Return to cart</Button>
                                    <Button variantColor="cta" size="lg">
                                        Continue to payment
                                    </Button>
                                </Stack>
                            </Box>
                        </Box>
                        <Box width={[1 / 4]} ml={4}>
                            <CartSummaryBox
                                lineItems={mockProducts}
                                shippingInfo={shippingInfo}
                                tax={tax}
                                discount={discount}
                                isSummaryOnly
                            />
                        </Box>
                    </Flex>
                </SiteWrapper>
            </Box>
            <Footer />
        </>
    ),
    "guest-payment-step": (
        <>
            <Header />
            <Box bg="white">
                <SiteWrapper>
                    <Box my={4}>
                        <Steps current={2}>
                            <Step title="Shipping" />
                            <Step title="Payment" />
                            <Step title="Order Review" />
                        </Steps>
                    </Box>
                    <Flex>
                        <Box width={[3 / 4]}>
                            <PaymentMethods />
                            <BillingAdress />
                            <Box mb={10}>
                                <Stack
                                    isInline
                                    my={4}
                                    alignItems="center"
                                    justifyContent="space-between"
                                >
                                    <Button>Return to cart</Button>
                                    <Button variantColor="cta" size="lg">
                                        Continue to payment
                                    </Button>
                                </Stack>
                            </Box>
                        </Box>
                        <Box width={[1 / 4]} ml={4}>
                            <CartSummaryBox
                                lineItems={mockProducts}
                                shippingInfo={shippingInfo}
                                tax={tax}
                                discount={discount}
                                isSummaryOnly
                            />
                        </Box>
                    </Flex>
                </SiteWrapper>
            </Box>
            <Footer />
        </>
    ),
}
