import { Box } from "@chakra-ui/core"
import React, { FC } from "react"

export const SiteWrapper: FC = ({ children }) => {
return <Box maxWidth={["78em"]} m={["auto"]}>{ children }</Box>
}