/* eslint-disable react/display-name */
import { ThemeProvider, CSSReset } from "@chakra-ui/core"
import React from "react"

import theme from "./theme/theme"
export default ({ children }) => (
    <ThemeProvider theme={theme}>
        <CSSReset />
        {children}
    </ThemeProvider>
)
