import {
    Box,
    Flex,
    Icon,
    Input,
    List,
    ListItem,
    PseudoBox,
    Text,
} from "@chakra-ui/core"
import React, { FC, useState } from "react"

import { MegaMenu } from ".."
import { SiteWrapper } from "../SiteWrapper"
import { Logo } from "./Logo"

const HeaderTopBar = () => {
    return (
        <Box bg="black" color="white" py={[4]}>
            <SiteWrapper>
                <Flex justifyContent="space-between">
                    <Flex width={4 / 5}>
                        <Logo />
                        <Box ml="12" borderLeft="solid 1px" paddingLeft="12">
                            <Text as="span">(800) 123 4562</Text>
                        </Box>
                    </Flex>
                    <Flex width={1 / 5} justifyContent="flex-end">
                        <Text as="span">Shop</Text>
                        <Text as="span" ml="4">
                            Managed Services
                        </Text>
                    </Flex>
                </Flex>
            </SiteWrapper>
        </Box>
    )
}

type miniCartProps = {
    itemNumber: number
}
const MiniCart: FC<miniCartProps> = ({ itemNumber }) => {
    return (
        <Box position="relative">
            <Text
                as="span"
                position="absolute"
                top="-8px"
                fontWeight="bold"
                color="red.700"
                display="block"
                width={
                    itemNumber > 99 ? "64px" : itemNumber > 10 ? "56px" : "60px"
                }
                textAlign="center"
                fontSize={
                    itemNumber > 99 ? "sm" : itemNumber > 10 ? "lg" : "2xl"
                }
            >
                {itemNumber > 99 ? "99+" : itemNumber}
            </Text>
            {/* 
                                // @ts-ignore */}
            <Icon name="cart" size="48px" />
        </Box>
    )
}

type SearchBarProps = {
    recentSearches?: string[]
}

const SearchBar: FC<SearchBarProps> = ({ recentSearches }) => {
    const [isRecentSearchesHidden, setRecentSearchesVisibility] = useState(
        false
    )
    return (
        <Box position="relative">
            <Input
                placeholder="Search"
                onFocus={() => setRecentSearchesVisibility(true)}
                onBlur={() => setRecentSearchesVisibility(false)}
            />
            <Box
                position="absolute"
                display={isRecentSearchesHidden ? "block" : "none"}
                boxShadow="lg"
                width="100%"
                marginTop={2}
                bg="white"
                borderRadius="sm"
            >
                <Text fontWeight="bold" margin="0" padding="4">
                    Recent Searches
                </Text>
                <List paddingLeft="0" marginY="0" paddingBottom="4">
                    {recentSearches?.map((item, index) => (
                        <PseudoBox
                            as="li"
                            paddingX="4"
                            paddingY="2"
                            _hover={{
                                backgroundColor: "gray.300",
                                cursor: "pointer"
                            }}
                            key={index}
                            lineHeight="tall"
                        >
                            {item}
                        </PseudoBox>
                    ))}
                </List>
            </Box>
        </Box>
    )
}

export const Header = () => {
    const recentSearches = ["windows 10", "office 365"]
    return (
        <Box as="header" fontFamily="body">
            <HeaderTopBar />
            <Box bg="white">
                <SiteWrapper>
                    <Flex alignItems="center">
                        <Box p={[4]} width={1 / 5}>
                            Shop
                        </Box>
                        <Box width={2 / 5} p={4}>
                            <SearchBar recentSearches={recentSearches} />
                        </Box>
                        <Flex
                            p={[4]}
                            width={2 / 5}
                            alignItems="center"
                            justifyContent="flex-end"
                        >
                            <Text as="span">
                                Sales & Support
                                <br />
                                <Text as="span">M-F 5AM - 5PM PT</Text>
                            </Text>
                            <Box
                                borderLeft="solid 1px"
                                paddingLeft="8"
                                marginLeft="8"
                            >
                                <MiniCart itemNumber={110} />
                            </Box>
                        </Flex>
                    </Flex>
                </SiteWrapper>
            </Box>
            <Box borderTop="solid 1px" bg="white">
                <SiteWrapper>
                    <MegaMenu />
                </SiteWrapper>
            </Box>
        </Box>
    )
}
