import {
    Box,
    Button,
    Flex,
    Input,
    Link,
    Text,
} from "@chakra-ui/core"
import React from "react"

import { SiteWrapper } from ".."

const schema = Object.assign(
    {},
    {
        links: [
            {
                "id": 0,
                "title": "My Account",
                "href": "#"
            },
            {
                "id": 1,
                "title": "View Cart",
                "href": "#"
            },            {
                "id": 2,
                "title": "Shipping & Returns",
                "href": "#"
            },
            {
                "id": 3,
                "title": "Payment Terms Application",
                "href": "#"
            },
            {
                "id": 4,
                "title": "Corporate",
                "href": "#"
            },
            {
                "id": 5,
                "title": "Government",
                "href": "#"
            },
            {
                "id": 6,
                "title": "Affiliate Program",
                "href": "#"
            },            {
                "id": 7,
                "title": "FAQ",
                "href": "#"
            },
            {
                "id": 8,
                "title": "Support Videos",
                "href": "#"
            },
            {
                "id": 9,
                "title": "E-mail Support",
                "href": "#"
            },
            {
                "id": 10,
                "title": "Remote Support",
                "href": "#"
            },
            {
                "id": 11,
                "title": "Subscription Management",
                "href": "#"
            },
            {
                "id": 12,
                "title": "About Us",
                "href": "#"
            },            {
                "id": 13,
                "title": "Meet The Team",
                "href": "#"
            },
            {
                "id": 14,
                "title": "Reviews",
                "href": "#"
            },
            {
                "id": 15,
                "title": "Blog",
                "href": "#"
            }
        ],
    }
)

const colOneLinks = schema.links.filter(
    item => [0, 1, 2, 3].indexOf(item.id) !== -1
)
const colTwoLinks = schema.links.filter(
    item => [4, 5, 6].indexOf(item.id) !== -1
)
const colThreeLinks = schema.links.filter(
    item => [7,8,9,10,11].indexOf(item.id) !== -1
)
const colFourLinks = schema.links.filter(
    item => [12,13,14,15].indexOf(item.id) !== -1
)

export const Footer = () => {
    return (
        <Box fontFamily="body" bg="white">
            <Flex
                display="flex"
                borderY="solid 1px"
                borderColor="gray.300"
                paddingY={6}
                justifyContent="center"
                alignItems="center"
            >
                <Text fontSize="lg" marginRight="4">
                    Stay up to date with the latest deals!
                </Text>
                <Flex alignItems="center">
                    <Input
                        placeholder="Enter email address"
                        width="360px"
                        marginRight="4"
                        borderColor="gray.300"
                    />
                    <Button variantColor="cta" size="md">
                        Sign Up
                    </Button>
                </Flex>
            </Flex>
            <SiteWrapper>
                <Flex paddingY="16" justifyContent="space-between">
                    <Box width={1 / 5}>
                        <Text as="h4">Shopping </Text>
                        {colOneLinks.map((item, index) => (
                            <Link
                                key={index}
                                href={item.href}
                                display="block"
                                color="black"
                                marginY="4"
                            >
                                {item.title}
                            </Link>
                        ))}
                    </Box>
                    <Box width={1 / 5}>
                        <Text as="h4">Enterprise </Text>
                        {colTwoLinks.map((item, index) => (
                            <Link
                                key={index}
                                href={item.href}
                                display="block"
                                color="black"
                                marginY="4"
                            >
                                {item.title}
                            </Link>
                        ))}
                    </Box>
                    <Box width={1 / 5}>
                        <Text as="h4">Get Help </Text>
                        {colThreeLinks.map((item, index) => (
                            <Link
                                key={index}
                                href={item.href}
                                display="block"
                                color="black"
                                marginY="4"
                            >
                                {item.title}
                            </Link>
                        ))}
                    </Box>
                    <Box width={1 / 5}>
                        <Text as="h4">Company </Text>
                        {colFourLinks.map((item, index) => (
                            <Link
                                key={index}
                                href={item.href}
                                display="block"
                                color="black"
                                marginY="4"
                            >
                                {item.title}
                            </Link>
                        ))}
                    </Box>
                    <Box width={1 / 5}>
                        <Text as="h4">Get Connected</Text>
                        Custom Content
                    </Box>
                </Flex>
            </SiteWrapper>
            <SiteWrapper>
                <Text textAlign="center">© 2020 Embur. All rights reserved.</Text>
            </SiteWrapper>
        </Box>
    )
}
