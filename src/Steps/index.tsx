import { Flex, Icon, PseudoBox, Text } from "@chakra-ui/core"
import React, { FC, ReactElement, useState } from "react"

type StepsProps = {
    current: number
    children: Array<ReactElement>
    onClick?: () => void
}

type StepProps = {
    number: number
    title: string
    description?: string
    isActive: boolean
    isLastChild: boolean
    onClick?: () => void
}

export const Steps: FC<StepsProps> = ({ current, children }) => {
    const [currentIndex, setCurrentIndex] = useState(current - 1)
    const renderSteps = () =>
        React.Children.map(children, (child: ReactElement, i) => {
            return child ? (
                <Step
                    key={i}
                    number={i + 1}
                    isActive={currentIndex === i}
                    isLastChild={i === children.length - 1}
                    title={(child.props as StepProps).title}
                />
            ) : null
        })
    return <Flex alignItems="center">{renderSteps()}</Flex>
}

export const Step: FC<StepProps> = ({
    number,
    title,
    isActive,
    isLastChild,
    onClick,
}) => {
    return (
        <PseudoBox
            py={4}
            textAlign="left"
            onClick={onClick}
            _hover={{
                cursor: isActive ? "default" : "pointer",
            }}
        >
            <Flex alignItems="center">
                <Text
                    bg={isActive ? "yellow.300" : "black"}
                    color={isActive ? "black" : "white"}
                    px={3}
                    py={2}
                    mr={3}
                    fontWeight="bold"
                >
                    {number}
                </Text>
                <Text as="span">{title}</Text>
                <Icon
                    name="chevron-right"
                    size="24px"
                    mx={4}
                    display={isLastChild ? "none" : "inline-block"}
                />
            </Flex>
        </PseudoBox>
    )
}
