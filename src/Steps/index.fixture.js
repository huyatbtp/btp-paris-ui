import React from "react"

import { Step, Steps } from "./"

export default {
    Basic: (
        <Steps current={2}>
            <Step title="Cart" />
            <Step
                title="Shipping & Payment"
              
            />
            <Step title="Place Order" />
        </Steps>
    ),
}
