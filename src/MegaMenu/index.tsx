import {
    Box,
    Icon,
    List,
    ListIcon,
    ListItem,
    PseudoBox,
    Text,
} from "@chakra-ui/core"
import React, { FC, useState } from "react"

const schema = {
    categories: [
        {
            id: 1,
            title: "Antivirus",
            subCategories: [4, 5, 17, 18, 19, 20],
            adBlockType: "link-list",
        },
        {
            id: 2,
            title: "Exchange",
            subCategories: [4, 5, 17, 18, 19, 20],
        },
        {
            id: 3,
            title: "Hardware",
            subCategories: [],
        },
        {
            id: 4,
            title: "Office",
            subCategories: [4, 5, 17, 18, 19, 20],
            adBlockType: "valentine",
        },
        {
            id: 5,
            title: "Other Software",
            subCategories: [1, 2],
        },
        {
            id: 6,
            title: "Project",
            subCategories: [],
        },
        {
            id: 7,
            title: "Sharepoint",
            subCategories: [],
        },
        {
            id: 8,
            title: "SQL Server",
            subCategories: [4, 5, 17, 18, 19, 20],
        },
        {
            id: 9,
            title: "Windows",
            subCategories: [4, 5, 17, 18, 19, 20],
        },
        {
            id: 10,
            title: "Windows Server",
            subCategories: [4, 5, 17, 18, 19, 20],
        },
        {
            id: 11,
            title: "Other Products",
            subCategories: [],
        },
        {
            id: 12,
            title: "365",
            subCategories: [],
        },
        {
            id: 13,
            title: "Home & Business",
            subCategories: [],
        },
        {
            id: 14,
            title: "Home and Student",
            subCategories: [],
        },
        {
            id: 15,
            title: "Professional",
            subCategories: [],
        },
        {
            id: 16,
            title: "Professional Plus",
            subCategories: [],
        },
        {
            id: 17,
            title: "Office",
            subCategories: [12, 13, 14, 15, 16],
        },
        {
            id: 18,
            title: "Other Software",
            subCategories: [1, 2, 3],
        },
        {
            id: 19,
            title: "Office",
            subCategories: [12, 13, 14, 15, 16],
        },
        {
            id: 20,
            title: "Other Software",
            subCategories: [1, 2, 3],
        },
    ],
}

const menuSettings = {
    mainMenuTreeWidth: 180,
    subMenuTreeWidth: 180,
}

const levelOneCategories = [4, 9, 10, 8, 1, 2, 5, 3, 11]

type CustomAdBlockProps = {
    type?: string
}
const CustomAdBlock: FC<CustomAdBlockProps> = ({ type }) => {
    switch (type) {
        case "link-list":
            return (
                <Box width={menuSettings.subMenuTreeWidth}>
                    <Text>Ad Block #1</Text>
                    <List spacing={3}>
                        <ListItem>
                            <ListIcon icon="check-circle" color="green.500" />
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit
                        </ListItem>
                        <ListItem>
                            <ListIcon icon="check-circle" color="green.500" />
                            Assumenda, quia temporibus eveniet a libero incidunt
                            suscipit
                        </ListItem>
                    </List>
                </Box>
            )
        case "valentine":
            return (
                <Box width={menuSettings.subMenuTreeWidth}>
                    <Text>Valentine Ad Block</Text>
                    <List spacing={3}>
                        <ListItem>
                            <ListIcon icon="check-circle" color="red.500" />
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit
                        </ListItem>
                        <ListItem>
                            <ListIcon icon="check-circle" color="red.500" />
                            Assumenda, quia temporibus eveniet a libero incidunt
                            suscipit
                        </ListItem>
                        <ListItem>
                            <ListIcon icon="check-circle" color="red.500" />
                            Assumenda, quia temporibus eveniet a libero incidunt
                            suscipit
                        </ListItem>
                    </List>
                </Box>
            )
        default:
            return null
    }
}

type MenuTreeItemProps = {
    id: number
    title: string
    subCategories: number[]
    adBlockType?: string
}

const MenuTreeChildItem: FC<MenuTreeItemProps> = ({
    title,
    subCategories,
}) => {
    const levelThreeItems = schema.categories.filter(
        item => subCategories.indexOf(item.id) !== -1
    )
    return (
        <PseudoBox width={menuSettings.subMenuTreeWidth} padding={4}>
            <Text fontWeight="bold">{title}</Text>
            {levelThreeItems.map((item, index) => (
                <PseudoBox
                    key={index}
                    _hover={{
                        color: "blue.700",
                    }}
                >
                    {item.title}
                </PseudoBox>
            ))}
        </PseudoBox>
    )
}

const MenuTreeItem: FC<MenuTreeItemProps> = ({
    title,
    subCategories,
    adBlockType,
}) => {
    const [displaySubTree, setDisplaySubTree] = useState(false)
    const levelTwoItems = schema.categories.filter(
        item => subCategories.indexOf(item.id) !== -1
    )
    return (
        <PseudoBox>
            <PseudoBox
                bg="gray.200"
                px={3}
                onMouseEnter={() => setDisplaySubTree(true)}
                onMouseLeave={() => setDisplaySubTree(false)}
                color={displaySubTree ? "blue.700" : "black"}
                display="flex"
                alignItems="center"
            >
                <Text as="span" marginRight="auto" marginY={3}>
                    {title}
                </Text>
                <Icon name="chevron-right" size="24px" color="gray.400" />
            </PseudoBox>
            <PseudoBox
                position="absolute"
                bg="white"
                top="0"
                left={menuSettings.mainMenuTreeWidth}
                display={
                    displaySubTree && levelTwoItems.length > 0 ? "flex" : "none"
                }
                flexWrap="wrap"
                onMouseEnter={() => setDisplaySubTree(true)}
                onMouseLeave={() => setDisplaySubTree(false)}
                width={menuSettings.subMenuTreeWidth * (adBlockType ? 4 : 3)}
                boxShadow="md"
            >
                <Box
                    display="flex"
                    flexWrap="wrap"
                    width={menuSettings.subMenuTreeWidth * 3}
                >
                    {levelTwoItems.map((item: MenuTreeItemProps, index) => (
                        <MenuTreeChildItem
                            id={item.id}
                            title={item.title}
                            subCategories={item.subCategories}
                            key={index}
                        />
                    ))}
                </Box>
                <CustomAdBlock type={adBlockType} />
            </PseudoBox>
        </PseudoBox>
    )
}

export const MegaMenu = () => {
    const [displayMegaMenu, setDisplayMegaMenu] = useState(false)
    const levelOneItems = schema.categories.filter(
        item => levelOneCategories.indexOf(item.id) !== -1
    )
    return (
        <PseudoBox>
            <PseudoBox
                bg="black"
                color="white"
                p={3}
                onMouseEnter={() => setDisplayMegaMenu(true)}
                onMouseLeave={() => setDisplayMegaMenu(false)}
            >
                All Products
            </PseudoBox>
            <PseudoBox
                position="absolute"
                zIndex={5}
                cursor="pointer"
                width={menuSettings.mainMenuTreeWidth}
                lineHeight="tall"
                display={displayMegaMenu ? "block" : "none"}
                onMouseEnter={() => setDisplayMegaMenu(true)}
                onMouseLeave={() => setDisplayMegaMenu(false)}
            >
                {levelOneItems.map((item, index) => (
                    <MenuTreeItem
                        key={index}
                        id={item.id}
                        title={item.title}
                        subCategories={item.subCategories}
                        adBlockType={item.adBlockType}
                    />
                ))}
            </PseudoBox>
        </PseudoBox>
    )
}
