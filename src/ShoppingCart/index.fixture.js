import React from "react"

import { ShoppingCart } from "./"

const mockProducts = [
    {
        title: "MICROSOFT WINDOWS 10 PRO - 1 LICENSE",
        imgSrc: "https://cdn.shopify.com/s/files/1/0855/1446/products/windows-10-download-license-32-64-bit.png?v=1563587921",
        price: "$129.99",
        qty: 1
    },
    {
        title: "MICROSOFT OFFICE 2019 HOME AND STUDENT LICENSE ENGLISH",
        imgSrc: "https://cdn.shopify.com/s/files/1/0855/1446/products/MS-Office-H_S-2019-Win-V2_b486a292-a623-45ba-b504-ffd3b14a14ea_large.png?v=1563416908",
        price: "$109.99",
        qty: 1
    },
    {
        title: "MICROSOFT OFFICE HOME AND STUDENT 2019 LICENSE FOR MAC",
        imgSrc: "https://cdn.shopify.com/s/files/1/0855/1446/products/hs19m-1_large.jpg?v=1574281075",
        price: "$329.99",
        qty: 1
    }
]

export default {
    Basic: (
        <ShoppingCart lineItems={mockProducts}/>
    ),
    "Cart is empty": (
        <ShoppingCart />
    ),
}