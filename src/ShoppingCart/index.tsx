import {
    Box,
    Flex,
    Heading,
    Image,
    Input,
    PseudoBox,
    Text,
} from "@chakra-ui/core"
import React, { FC, useState } from "react"

type ProductProps = {
    title: string
    imgSrc: string
    qty: number
    price: string
}

type CartItemProps = {
    removeCartItem: (title: string) => void
}
type ShoppingCartProps = {
    lineItems: Array<ProductProps>
}

export const CartItem: FC<ProductProps & CartItemProps> = ({
    title,
    imgSrc,
    qty,
    price,
    removeCartItem,
}) => {
    return (
        <Box py="4">
            <Flex my="4">
                <Box width="180px">
                    <Image width="150px" mr="4" src={imgSrc}></Image>
                </Box>
                <Box width={7/8}>
                    <Flex>
                        <Box
                            as="span"
                            fontFamily="body"
                            fontSize="lg"
                            fontWeight="bold"
                            marginRight="auto"
                        >
                            {title}
                        </Box>
                        <Box
                            as="span"
                            fontFamily="body"
                            marginLeft="auto"
                            fontWeight="bold"
                            fontSize="lg"
                            color="red.600"
                        >
                            {price}
                        </Box>
                    </Flex>

                    <Flex alignItems="center" my="4">
                        <Flex alignItems="center">
                            <Box as="span" fontFamily="body" mr="2">
                                Qty:
                            </Box>
                            <Input
                                placeholder={qty.toString()}
                                type="number"
                                size="sm"
                                min="1"
                            />
                        </Flex>
                        <Box borderLeft="solid 1px black" pl="8" ml="8">
                            <PseudoBox
                                as="span"
                                fontFamily="body"
                                fontSize="sm"
                                _hover={{
                                    cursor: "pointer",
                                }}
                                onClick={() => removeCartItem(title)}
                            >
                                Delete
                            </PseudoBox>
                        </Box>
                    </Flex>
                </Box>
            </Flex>
        </Box>
    )
}

const CartNoItems: FC = () => {
    return (
        <Box>
            <Heading fontSize="md">Your Shopping Cart is empty</Heading>
            <Text>
                You don’t have any items in your cart. Start shopping now.
            </Text>
        </Box>
    )
}

export const ShoppingCart: FC<ShoppingCartProps> = ({ lineItems }) => {
    const [items, setItems] = useState(lineItems)
    const removeCartItem = (title: string) =>
        setItems(items.filter(item => item.title !== title))
    return (
        <Box>
            <Flex
                alignItems="center"
                justifyContent="space-between"
                borderBottom="solid 1px black"
                py={1}
            >
                <Heading as="h2" fontSize="2xl">
                    Shopping Cart
                </Heading>
                <Text as="span">Price</Text>
            </Flex>
            {items && items.length > 0 ? (
                items.map((item, index) => (
                    <CartItem
                        key={index}
                        title={item.title}
                        imgSrc={item.imgSrc}
                        price={item.price}
                        qty={item.qty}
                        removeCartItem={removeCartItem}
                    />
                ))
            ) : (
                <CartNoItems />
            )}
        </Box>
    )
}
