import { Box, Button, Flex,Input } from "@chakra-ui/core"
import React, { useState } from "react"

export const ProductQuantitySelector = () => {
    const [qty, setQty] = useState(1)
    const [qtyInput, setQtyInput] = useState("10")
    return (
        <Box>
            {qty < 10 ? (
                <Input
                    as="select"
                    appearance="none"
                    defaultValue={qty}
                    onChange={e => setQty(e.target.value)}
                >
                    {Array.from(Array(11), (_, i) => (
                        <option value={i} key={i}>
                            {i === 0 ? "0 (delete)" : i === 10 ? "10++" : i}
                        </option>
                    ))}
                </Input>
            ) : (
                <Flex>
                    <Input type="text" value={qtyInput} onKeyUp={e => console.log(e.target.value.replace(/[A-Z]/g,""))} onChange={e => setQtyInput(e.target.value)}/>
                    <Button onClick={() => setQty(qty)}>Update</Button>
                </Flex>
            )}
        </Box>
    )
}
