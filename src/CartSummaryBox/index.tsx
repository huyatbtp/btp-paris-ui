import { Box, Button, Flex, Heading } from "@chakra-ui/core"
import _ from "lodash"
import React, { FC } from "react"

type ProductProps = {
    title: string
    imgSrc: string
    qty: number
    price: number
}

type CartSummaryProps = {
    isSummaryOnly: boolean
    lineItems: Array<ProductProps>
    shippingInfo: {
        type: string
        price: string
    }
    tax: number
    discount: {
        code: string
        value: number
    }
}

export const CartSummaryBox: FC<CartSummaryProps> = ({
    lineItems,
    shippingInfo,
    tax,
    isSummaryOnly,
}) => {
    const itemsValues = lineItems.map(item => item.price)
    const total = itemsValues.reduce((sum, item) => sum + item)
    const taxTotal = _.round(total * tax, 2)
    const subTotal = total + taxTotal

    return (
        <Box border="solid 1px black" borderRadius="md">
            {isSummaryOnly ? null : (
                <Box p={4} borderBottom="solid 1px black">
                    <Button variantColor="cta" width="100%">
                        Proceed to Checkout
                    </Button>
                </Box>
            )}
            <Box p={4}>
                <Heading as="h3" fontSize="2xl" mt="0">
                    Summary
                </Heading>
                <Flex justifyContent="space-between">
                    <Box
                        as="span"
                        fontSize="sm"
                        display="block"
                        my={1}
                        fontFamily="body"
                    >
                        Subtotal ({lineItems.length} items)
                    </Box>
                    <Box
                        as="span"
                        fontSize="sm"
                        display="block"
                        my={1}
                        fontFamily="body"
                        fontWeight="bold"
                    >
                        ${total}
                    </Box>
                </Flex>
                <Flex justifyContent="space-between">
                    <Box
                        as="span"
                        fontSize="sm"
                        display="block"
                        my={1}
                        fontFamily="body"
                    >
                        Shipping
                    </Box>
                    <Box
                        as="span"
                        fontSize="sm"
                        display="block"
                        my={1}
                        fontFamily="body"
                    >
                        {shippingInfo.type} ({shippingInfo.price})
                    </Box>
                </Flex>
                <Flex justifyContent="space-between">
                    <Box
                        as="span"
                        fontSize="sm"
                        display="block"
                        my={1}
                        fontFamily="body"
                    >
                        Total before taxes
                    </Box>
                    <Box
                        as="span"
                        fontSize="sm"
                        display="block"
                        my={1}
                        fontFamily="body"
                    >
                        $539.98
                    </Box>
                </Flex>
                <Flex justifyContent="space-between">
                    <Box
                        as="span"
                        fontSize="sm"
                        display="block"
                        my={1}
                        fontFamily="body"
                    >
                        Sales Tax
                    </Box>
                    <Box
                        as="span"
                        fontSize="sm"
                        display="block"
                        my={1}
                        fontFamily="body"
                    >
                        ${taxTotal}
                    </Box>
                </Flex>
                <Flex
                    justifyContent="space-between"
                    alignItems="center"
                    mt="4"
                    borderTop="solid 1px gray"
                    py="4"
                >
                    <Box
                        as="span"
                        fontFamily="body"
                        fontSize="lg"
                        fontWeight="bold"
                    >
                        Order total:
                    </Box>
                    <Box
                        as="span"
                        fontFamily="body"
                        fontSize="lg"
                        fontWeight="bold"
                    >
                        ${subTotal}
                    </Box>
                </Flex>
            </Box>
        </Box>
    )
}
