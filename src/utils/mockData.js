export const shippingInfo = {
    type: "Digital Delivery",
    price: "$0.00",
}

export const tax = 0.1
export const discount = {
    code: "HELLO2020",
    value: 0.1,
}

export const mockProducts = [
    {
        title: "MICROSOFT WINDOWS 10 PRO - 1 LICENSE",
        imgSrc:
            "https://cdn.shopify.com/s/files/1/0855/1446/products/windows-10-download-license-32-64-bit.png?v=1563587921",
        price: 129.99,
        qty: 1,
    },
    {
        title: "MICROSOFT OFFICE 2019 HOME AND STUDENT LICENSE ENGLISH",
        imgSrc:
            "https://cdn.shopify.com/s/files/1/0855/1446/products/MS-Office-H_S-2019-Win-V2_b486a292-a623-45ba-b504-ffd3b14a14ea_large.png?v=1563416908",
        price: 109.99,
        qty: 1,
    },
    {
        title: "MICROSOFT OFFICE HOME AND STUDENT 2019 LICENSE FOR MAC",
        imgSrc:
            "https://cdn.shopify.com/s/files/1/0855/1446/products/hs19m-1_large.jpg?v=1574281075",
        price: 329.99,
        qty: 1,
    },
]

export const shippingAddress = [
    {
        address: {
            firstname: "John",
            lastname: "Doe",
            company: "Company Name",
            street: ["320 N Crescent Dr", "Beverly Hills"],
            city: "Los Angeles",
            region: "CA",
            postcode: "90210",
            country_code: "US",
            telephone: "123-456-0000",
            save_in_address_book: false,
        },
    },
]

export const availablePaymentMethods = [
    {
        code: "card",
        title: "Credit card",
    },
    {
        code: "checkmo",
        title: "Check / Money order",
    },
]
